import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { map, exhaustMap, delay, mergeMap, flatMap } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { interval } from 'rxjs/observable/interval';
import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/Observable';
import { merge } from 'rxjs/observable/merge';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public buses: Bus[] = [
    new Bus('184')
  ];

  public busStops: BusStop[] = [
    { name: 'Stop 1', xPosition: 0, passengers: [] },
    { name: 'Stop 1', xPosition: 918, passengers: [] },
  ];

  public passengerName: string;

  public messages: string[] = [];
  public logMessage(message: string) {
    this.messages.push(message);
    this.messages = this.messages.reverse().slice(0, 5).reverse();
  }

  newPassenger$: Subject<Passenger> = new Subject<Passenger>();
  typedPassenger$: Subject<Passenger> = new Subject<Passenger>();
  fakedPassenger$: Observable<Passenger>;

  // http://faker.hook.io?property=name.firstName&locale=pl

  constructor(private httpClient: Http) {}

  ngOnInit(): void {
    this.fakedPassenger$ = interval(30000).pipe(
      flatMap(p => this.httpClient.get('http://faker.hook.io?property=name.firstName&locale=pl')),
      map(response => response.json()),
      map(name => new Passenger(name))
    );

    merge(this.typedPassenger$, this.fakedPassenger$)
      .subscribe(this.newPassenger$)

    this.newPassenger$.subscribe(passenger => this.busStops[0].passengers.push(passenger));
  }

  addPassenger() {
    this.typedPassenger$.next(new Passenger(this.passengerName));
    this.passengerName = null;
  }

  passengerNames = (bus: Bus) => bus.passengers.map(p => p.name).join(', ');

}

export class Bus {
  constructor(public name: string) {}
  passengers: Passenger[] = [];
  direction = 1;
  speed = 50; // pixels per second
  isStopped = true;
  xPosition = 0;
}

export class Passenger {
  constructor(public name: string) {}
  direction = 1;
}

export class BusStop {
  constructor(public name: string) { }
  xPosition: number;
  passengers: Passenger[];
}
