import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Passenger } from '../app.component';
import { interval } from 'rxjs/observable/interval';
import { animationFrame } from 'rxjs/scheduler/animationFrame';
import { bufferCount, tap } from 'rxjs/operators';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-passenger',
  templateUrl: './passenger.component.html',
  styleUrls: ['./passenger.component.css']
})
export class PassengerComponent implements OnInit, OnDestroy {

  @Input()
  passenger: Passenger;

  private sub: Subscription;

  constructor() { }

  ngOnInit() {
    this.sub = interval(2000)
      .pipe(
        tap(() => this.passenger.direction = - this.passenger.direction)
      ).subscribe();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  url = () => {
    if (this.passenger.name === 'Kasia') {
      return 'assets/kasia.png';
    } else if (this.passenger.name.endsWith('a')) {
      return 'assets/buddyGirl.png';
    } else {
      return 'assets/buddy.png';
    }
  }

}
