import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Bus, BusStop, Passenger } from '../app.component';

import { of } from 'rxjs/observable/of';
import { interval } from 'rxjs/observable/interval';
import { merge } from 'rxjs/observable/merge';
import { animationFrame } from 'rxjs/scheduler/animationFrame';
import { filter, map, tap, mergeMap, exhaustMap, take, startWith, delay,
  distinctUntilChanged, switchMap, takeWhile, takeUntil, concatAll, concatMap, buffer, bufferCount, finalize, distinct, pairwise } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-bus',
  templateUrl: './bus.component.html',
  styleUrls: ['./bus.component.css']
})
export class BusComponent implements OnInit {

  @Input()
  bus: Bus;

  @Input()
  busStops: BusStop[];

  @Output()
  newMessage: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    interval(0, animationFrame).pipe(
      map(() => Date.now()),
      pairwise(),
      map(([t1, t2]) => t2 - t1),
      map(dt => dt / 1000),
      tap(dt => this.bus.xPosition += this.bus.speed * dt * this.bus.direction),
      map(() => this.bus.xPosition),
      filter(x => x > 1000 - 82 || x < 0),
      tap(() => this.bus.direction = - this.bus.direction)
    ).subscribe();
  }

  toggle = () => {
    
  }

  log(message: string) {
    this.newMessage.emit(`[ Bus ${this.bus.name} ]: ${message}`);
  }

  currentBusStop = () => this.busStops.find(s => s.xPosition === this.bus.xPosition);
  isLastBusStop = () => {
    const stop = this.currentBusStop();
    return stop && this.busStops.indexOf(stop) === 0 || this.busStops.indexOf(stop) === this.busStops.length - 1;
  }
}
